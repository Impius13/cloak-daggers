class Event:
	def __init__(self, active, text=None, items=None, positionChange=None,
		tileToMod=None, tileUnblock=False, tileActivate=False):
	
		self.active = active
		self.text = text
		self.items = items
		self.positionChange = positionChange
		# Another tile to modify
		self.tileToMod = tileToMod
		self.tileUnblock = tileUnblock
		self.tileActivate = tileActivate

	def __text(self):
		# Play interactive text
		text = self.text.split("~")
		# Split the text where symbol '~' is and print those parts solo
		for part in text:
			print("\n" + part)
			input("~")
		print("\n")

	def __dropItems(self, player):
		# Give items to player
		for item in self.items:
			player.inventory.addItem(item)

	def __changePosition(self, player):
		# Change the position of player
		player.x = self.positionChange[0]
		player.y = self.positionChange[1]

	def __modifyTile(self, tileMap):
		# Modify another tile
		if self.tileUnblock:
			if tileMap[self.tileToMod].eastBlocked:
				tileMap[self.tileToMod].eastBlocked = False

			if tileMap[self.tileToMod].westBlocked:
				tileMap[self.tileToMod].westBlocked = False

			if tileMap[self.tileToMod].northBlocked:
				tileMap[self.tileToMod].northBlocked = False

			if tileMap[self.tileToMod].southBlocked:
				tileMap[self.tileToMod].southBlocked = False

		if self.tileActivate:
			tileMap[self.tileToMod].events[0].active = True

	def playEvent(self, player=None, tileMap=None):
		# Play the event
		if self.text:
			self.__text()

		if self.items:
			self.__dropItems(player)

		if self.positionChange:
			self.__changePosition(player)

		if self.tileToMod:
			self.__modifyTile(tileMap)