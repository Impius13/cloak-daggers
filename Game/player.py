import random
import inventory
from items import Weapon, Potion

class Player:
	def __init__(self):
		# !!!!! ADD STAT POINTS !!!!!
		self.x = 7
		self.y = 14

		self.level = 1
		self.maxHp = 10
		self.hp = self.maxHp
		self.xp = 0
		self.xpNeeded = 50

		self.weapon = None
		self.inventory = inventory.Inventory()

	def isAlive(self):
		# Check if player is alive
		return self.hp > 0

	def levelUp(self):
		# Increase player's level and hit points
		if self.xp >= self.xpNeeded:
			self.level += 1
			self.xp -= self.xpNeeded
			
			if self.level == 2:
				self.maxHp = 15
				self.hp = self.maxHp
				self.xpNeeded = 100

			elif self.level == 3:
				self.maxHp = 23
				self.hp = self.maxHp
				self.xpNeeded = 200

			elif self.level == 4:
				self.maxHp = 35
				self.hp = self.maxHp
				self.xpNeeded = 400

			elif self.level == 5:
				self.maxHp = 53
				self.hp = self.maxHp
				self.xpNeeded = 800

			elif self.level == 6:
				self.maxHp = 80
				self.hp = self.maxHp
				self.xpNeeded = 1000

			elif self.level == 7:
				self.maxHp = 120
				self.hp = self.maxHp
				self.xpNeeded = 1500

			elif self.level == 8:
				self.maxHp = 200
				self.hp = self.maxHp
				self.xpNeeded = 0

			else:
				return ""
			return "\nYou levelled up!\nHP increased to {}".format(self.hp)

		return ""

	def doAction(self, hotkey, *args):
		# Do action by given hotkey
		hotkey = hotkey.lower()
		if hotkey == "e":
			self.__moveEast()

		elif hotkey == "w":
			self.__moveWest()

		elif hotkey == "n":
			self.__moveNorth()

		elif hotkey == "s":
			self.__moveSouth()

		elif hotkey == "a":
			return self.__attack(*args)

		elif hotkey == "d":
			return self.__defend(*args)

		elif hotkey == "i":
			return self.__listInventory()

		elif hotkey == "u":
			return self.__use()

		elif hotkey == "b":
			self.__buy(*args)

		elif hotkey == "l":
			self.__sell(*args)

		else:
			return None

	def handleInput(self, validActions):
		# List available actions and handle player input
		print("\nAvailable actions:")
		for action in validActions.items():
			print(action[1])

		# Wait for player input and handle it
		while True:
			playerAction = input("\nYour action: ").lower()
			if playerAction in validActions.keys():
				return playerAction
			else:
				print("\nI can't do that!")

	# Movement actions
	def __moveEast(self):
		self.x += 1

	def __moveWest(self):
		self.x -= 1

	def __moveNorth(self):
		self.y -= 1

	def __moveSouth(self):
		self.y += 1

	# Fighting actions
	def __attack(self, enemy):
		dealtDamage = random.randint(self.weapon.minDmg, self.weapon.maxDmg)
		enemy.hp -= dealtDamage
		return "\nYou've attacked {} and dealt {} damage!".format(enemy.name, dealtDamage)

	def __defend(self, enemy):
		receivedDamage = int(enemy.calcDmg() / 2)
		self.hp -= receivedDamage
		enemy.playerDefense()
		return "\nYou've defended against {} attack and received {} damage!".format(enemy.name, receivedDamage)

	# Inventory actions
	def __listInventory(self):
		# List items in player's inventory
		return self.inventory.listItems()

	def __use(self):
		# Use item or equip
		mark = input("\nWhat do you want to use or equip? (enter item mark or press 'z' to exit): ")
		if mark.lower() == "z":
			# Player decided to exit
			return ""

		item = self.inventory.getItem(mark)
		if item == None:
			return "\nYou don't have that."

		if isinstance(item, Weapon):
			# If item is weapon, equip it
			if self.weapon:
				self.inventory.items[self.weapon.mark].active = False
			self.inventory.items[mark].active = True
			self.weapon = item
			return "\nYou've equipped {}.".format(self.weapon.name.lower())

		elif isinstance(item, Potion):
			# If item is potion, use it
			self.hp += item.hp
			if self.hp >= self.maxHp:
				self.hp = self.maxHp
				
			self.inventory.removeItem(mark)
			return "\nYou've consumed {} and restored {} HP.".format(item.name.lower(), item.hp)

		else:
			return "\nYou can't use that."

	# Shopping actions
	def __buy(self, item):
		self.inventory.addItem(item)
		self.inventory.removeGold(item.value)

	def __sell(self, mark, gold):
		self.inventory.removeItem(mark)
		self.inventory.addGold(gold)
