from items import Weapon

class Inventory:
	def __init__(self, items=None):
		self.marks = ["a","b","c","d","e","f","g","h","i","j","k"]
		self.items = {}
		if items:
			# Add starting items to inventory
			for item in items:
				self.addItem(item)

	def getItem(self, mark):
		# Check if inventory have item with given mark
		return self.items.get(mark.lower())

	def getGold(self):
		# Check amount of gold
		return self.items.get("a").amount

	def addItem(self, item):
		# Add item to inventory
		exists = False
		for mark, invItem in self.items.items():
			# Check if item is already in inventory
			if item == invItem:
				self.items[mark].amount += item.amount
				exists = True
		# Item key will be first entry in marks
		if not exists:
			item.mark = self.marks[0]
			self.items[self.marks[0]] = item
			# Delete the key form marks
			del self.marks[0]

	def addGold(self, amount):
		# Add gold to inventory
		self.items["a"].amount += amount

	def removeItem(self, mark, amount=1):
		# Remove item from inventory
		if self.getItem(mark):
			if self.items[mark].amount > amount:
				# Subtract just from amount, don't remove the item
				self.items[mark].amount -= amount

			else:
				# Remove the item and add mark to available marks
				del self.items[mark]
				self.marks.append(mark)
				self.marks = sorted(self.marks)

	def removeGold(self, amount):
		# Remove gold from inventory
		if self.items["a"].amount >= amount:
			self.items["a"].amount -= amount

	def listItems(self):
		# List all items in inventory
		string = ""
		for mark, item in sorted(self.items.items()):
			string += "\n{}: ".format(mark)
			string += str(item)
			if isinstance(item, Weapon):
				if item.active == True:
					string += "Eqipped\n"
		return string

	def listItemsShop(self):
		# List items for shop
		string = ""
		for mark, item in sorted(self.items.items()):
			string += "\n{}: ".format(mark)
			string += item.strShop()
		return string