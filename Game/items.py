class Item:
	def __init__(self, name, description, value, amount):
		self.name = name
		self.description = description
		self.value = value
		self.amount = amount
		self.mark = None

	def __str__(self):
		return "\n{}\n====================\n{}\n\nValue: {}\nAmount: {}\n".format(self.name,
			self.description, self.value, self.amount)

	def strShop(self):
		return "{}\n====================\n{}\nPrice: {}\n".format(self.name, self.description,
			self.value)


class Weapon(Item):
	def __init__(self, name, description, value, minDmg, maxDmg, amount):
		super().__init__(name, description, value, amount)
		self.minDmg = minDmg
		self.maxDmg = maxDmg
		self.active = False

class Potion(Item):
	def __init__(self, name, description, value, hp, amount):
		super().__init__(name, description, value, amount)
		self.hp = hp


# ========== Items used in game ==========
# Weapons
class Axe(Weapon):
	def __init__(self, amount=1):
		super().__init__("Saviel's axe", "Saviel's old, trusty battle axe. It needs a little bit of sharpening.",
			50, 2, 3, amount)


class Quarterstaff(Weapon):
	def __init__(self, amount=1):
		super().__init__("Quarterstaff", "Long, polished staff with small iron bulge at end.", 100, 4, 6, amount)


class BattleAxe(Weapon):
	def __init__(self, amount=1):
		super().__init__("Battle axe", "Well crafted fighting axe with long handle and spike on backside.", 500, 8, 12, amount)


class Sword(Weapon):
	def __init__(self, amount=1):
		super().__init__("Broadsword", "Ordinary broadsword in a pretty good shape.", 400, 12, 17, amount)


class Longsword(Weapon):
	def __init__(self, amount=1):
		super().__init__("Longsword", "An excelent two-handed longsword with beautiful crossguard.", 1500, 20, 30, amount)


class EnchantedSword(Weapon):
	def __init__(self, amount=1):
		super().__init__("Enchanted sword", "Magically enchanted longsword. You can feel some kind of energy emitting from it.",
			0, 60, 100, amount)


# Potions
class Herbs(Potion):
	def __init__(self, amount=1):
		super().__init__("Healing herbs", "Quite common herbs that help with healing wounds.", 20, 10, amount)


class SmallPotion(Potion):
	def __init__(self, amount=1):
		super().__init__("Small healing potion", "Small dose of healing potion made from various herbs.", 40, 25, amount)


class Mushrooms(Potion):
	def __init__(self, amount=1):
		super().__init__("Healing mushrooms", "Pretty strong healing mushrooms. They taste horrible.", 80, 45, amount)


class HPotion(Potion):
	def __init__(self, amount=1):
		super().__init__("Healing potion", "Common dose of healing potion.", 100, 60, amount)


class StrongPotion(Potion):
	def __init__(self, amount=1):
		super().__init__("Strong healing potion", "Strongly concentrated healing potion. Almost magical.", 300, 200, amount)


# Misc
class Gold(Item):
	def __init__(self, amount=1):
		super().__init__("Gold", "Roughly made gold coin. A common currency. Every coin should have same weight.", 1, amount)


class Torch(Item):
	def __init__(self, amount=1):
		super().__init__("Torch", "A simple torch.", 0, amount)


class Ring(Item):
	def __init__(self, amount=1):
		super().__init__("Magic ring", "Ring that Auricius gave to you. With it, creatures with that black fluid can't see you.", 0, amount)


class Amulet(Item):
	def __init__(self, amount=1):
		super().__init__("Amulet of protection", "Amulet infused with strong protection spell. Magic attacks don't harm you as much.", 0, amount)