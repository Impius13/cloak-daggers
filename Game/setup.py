from cx_Freeze import setup, Executable

setup(name = "Cloak&Dagger",
	version = "0.1",
	description = "Text RPG",
	executables = [Executable("game.py")])