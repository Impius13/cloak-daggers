import random

class Enemy:
	def __init__(self, name, description, minHp, maxHp, minDmg, maxDmg,
		minXpDrop, maxXpDrop, minGold, maxGold=None):
		# Base enemy class
		self.name = name
		self.description = description

		self.hp = random.randint(minHp, maxHp)
		self.fullHp = self.hp

		self.minDmg = minDmg
		self.maxDmg = maxDmg

		self.minXpDrop = minXpDrop
		self.maxXpDrop = maxXpDrop
		self.minGold = minGold
		self.maxGold = maxGold

		self.playerDefended = False

	def __str__(self):
		return "\n{}\n====================\n{}\n".format(self.name, self.description)

	def strFight(self):
		return "\n{}\n{}\n".format(self.name, self.status())

	def status(self):
		# Return state of wounds on enemy determined by hp
		if self.hp <= 0:
			return "Dead"

		elif self.hp <= (self.fullHp * (1 / 4)):
			return "All torn up"

		elif self.hp <= (self.fullHp / 2):
			return "Hurt"

		elif self.hp <= (self.fullHp * (3 / 4)):
			return "Lightly wounded"

		elif self.hp <= self.fullHp:
			return "Healthy"

	def calcDmg(self):
		# Get random damage value from given range
		return random.randint(self.minDmg, self.maxDmg)

	def playerDefense(self):
		# Set playerDefended variable to True
		self.playerDefended = True

	def attack(self, player):
		# Damage the player
		if self.playerDefended:
			# If player defended, don't attack
			self.playerDefended = False
			return None

		dealtDamage = self.calcDmg()
		player.hp -= dealtDamage
		return "{} attacked you and dealt {} damage!".format(self.name, dealtDamage)

	def dropXp(self):
		# Drop random XP amount form given range
		return random.randint(self.minXpDrop, self.maxXpDrop)

	def dropGold(self):
		# Drop random gold amount from given range
		if self.maxGold:
			return random.randint(self.minGold, self.maxGold)

		return self.minGold


# ========== Enemy types in game ==========
# !!!!! CHANGE DESCRIPTION AND ADD DROPS !!!!!
class Rat(Enemy):
	def __init__(self):
		super().__init__("Rat", "Big ugly rat.", 4, 6, 1, 2, 4, 6, 10, 15)


class Goblin(Enemy):
	def __init__(self):
		super().__init__("Goblin", "Small, angry man.", 12, 18, 2, 4, 7, 10, 30, 45)


class Spider(Enemy):
	def __init__(self):
		super().__init__("Spider", "Giant spider!", 24, 36, 5, 8, 20, 35, 50, 60)


class Bat(Enemy):
	def __init__(self):
		super().__init__("Bat", "Monstrous bat. Looks like dracula.", 25, 40, 9, 12, 30, 60, 100, 200)


class StrangeCreature(Enemy):
	def __init__(self):
		super().__init__("Strange Creature", "Small, humanoid creature covered in thick black fluid.",
			40, 70, 15, 25, 80, 120, 200, 300)


class Golem(Enemy):
	def __init__(self):
		super().__init__("Golem", "Big colossus made from clay.", 200, 300, 20, 30, 100, 100, None)


class Elemental(Enemy):
	def __init__(self):
		super().__init__("Elemental", "One of the five elemantals, each symbolize one element.",
			120, 200, 40, 50, 100, 100, None)


# Bosses
# !!!!! ADD DROPS !!!!!
class Wolf(Enemy):
	def __init__(self):
		super().__init__("Lone wolf", "Old, wounded but experienced lone wolf with strong spirit.",
			25, 25, 2, 3, 30, 30, 100)


class Bear(Enemy):
	def __init__(self):
		super().__init__("Beast", "Hideous beast that once was a bear.",
			120, 120, 7, 12, 150, 150, 500)


class Johnny(Enemy):
	def __init__(self):
		super().__init__("The slayer", "Horrible creature with big claws.",
			150, 150, 20, 40, 500, 500, 1000)


class Mage(Enemy):
	def __init__(self):
		super().__init__("Auricius", "True monster and betrayer.", 666, 666, 50, 66, 0, 0, None)