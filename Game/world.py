import csv

import player
from tiles import *
from enemies import *
from items import *
from event import Event

tileMap = {}
player = player.Player()
player.inventory.addItem(Gold(10))
player.inventory.addItem(Axe())

def makeMap():
	# Generates the game map
	with open("data/map.csv") as csvFile:
		# Get map from csv file
		file = csv.reader(csvFile, delimiter=";")
		x = 0
		y = 0
		for row in file:
			# Loop through map file
			for column in row:
				if column == "None" or column == "ď»żNone" or column == "MeÄŤ":
					pass

				else:
					string = column.split("-")
					# Convert string name of tile class to python code
					tile = eval(string[0] + "()")
					# Modify blocking variables
					for arg in string:
						if arg == "westBlocked":
							tile.westBlocked = True

						elif arg == "eastBlocked":
							tile.eastBlocked = True

						elif arg == "northBlocked":
							tile.northBlocked = True

						elif arg == "southBlocked":
							tile.southBlocked = True
					# Change tile's coordinates to correct values
					tile.x = x
					tile.y = y
					# Add tile to world map
					tileMap[(x, y)] = tile
				x += 1

			y += 1
			x = 0

		# Add events to specific normal tiles
		tileMap[(17, 17)].addEvent(Event(active=True, text=tileMap[(17, 17)].textElem[1].text))
		tileMap[(10, 8)].addEvent(Event(active=True, text=tileMap[(10, 8)].textElem[1].text))
		tileMap[(13, 5)].ambushChance = 1
		tileMap[(13, 5)].addEvent(Event(active=True, text=tileMap[(13, 5)].textElem[1].text))
		tileMap[(13, 5)].addEvent(Event(active=True, text=tileMap[(13, 5)].textElem[2].text))
		tileMap[(20, 1)].addEvent(Event(active=False, text=tileMap[(20, 1)].textElem[3].text,
			tileToMod=(10, 9), tileActivate=True))

def tileExists(x, y):
	# Check if tile with given coordinates exist
	return tileMap.get((x, y))

def adjacentMoves(tile):
	# Return available move actions for adjacent tiles
	moves = {}
	otherTile = tileExists(tile.x + 1, tile.y)
	if otherTile:
		if not otherTile.westBlocked:
			moves["e"] = "Move east - e"

	otherTile = tileExists(tile.x - 1, tile.y)
	if otherTile:
		if not otherTile.eastBlocked:
			moves["w"] = "Move west - w"

	otherTile = tileExists(tile.x, tile.y + 1)
	if otherTile:
		if not otherTile.northBlocked:
			moves["s"] = "Move south - s"

	otherTile = tileExists(tile.x, tile.y - 1)
	if otherTile:
		if not otherTile.southBlocked:
			moves["n"] = "Move north - n"
	return moves


# ========== Tiles used in game ==========

# Friendly tiles
class SavielHouse(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text))


class Square(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text,
			items=[SmallPotion(3)]))


class VillageRoad(Tile):
	def __init__(self):
		super().__init__(0, 0)


class JohnnyHouse(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text))


class JohnnyField(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=False, text=self.textElem[1].text,
			items=[Gold(150)], tileToMod=(8, 16), tileUnblock=True))


class BalorgHouse(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text))


class CaveEntrance(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=False, text=self.textElem[1].text))


class CaveMage(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text,
			items=Ring(), tileToMod=(20, 1), tileActivate=True))


class GroundFloor(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text,
			tileToMod=(9,14), tileActivate=True))


class Bedroom(Tile):
	def __init__(self):
		super().__init__(0, 0)


# Hostile tiles
class WestForest(HostileTile):
	def __init__(self):
		super().__init__(0, 0, 5, [Rat()])


class EastForest(HostileTile):
	def __init__(self):
		super().__init__(0, 0, 5, [Goblin()])


class DeadForest(HostileTile):
	def __init__(self):
		super().__init__(0, 0, 5, [Spider()])


class Cave(HostileTile):
	def __init__(self):
		super().__init__(0, 0, 5, [Bat()])


class DeepCave(HostileTile):
	def __init__(self):
		super().__init__(0, 0, 5, [StrangeCreature()])


class Tower(HostileTile):
	def __init__(self):
		super().__init__(0, 0, 5, [Golem(), Elemental()])


# Boss tiles
class WolfDen(HostileTile):
	def __init__(self):
		super().__init__(0, 0, 1, [Wolf()])
		self.addEvent(Event(active=True, text=self.textElem[1].text))
		self.addEvent(Event(active=True, text=self.textElem[2].text))


class BearClearing(HostileTile):
	def __init__(self):
		super().__init__(0, 0, 1, [Bear()])
		self.addEvent(Event(active=True, text=self.textElem[1].text))
		self.addEvent(Event(active=True, text=self.textElem[2].text,
			tileToMod=(8, 12), tileUnblock=True))


class JohnnyFight(HostileTile):
	def __init__(self):
		super().__init__(0, 0, 1, [Johnny()])
		self.addEvent(Event(active=True, text=self.textElem[1].text))
		self.addEvent(Event(active=True, text=self.textElem[2].text,
			items=[Amulet(), EnchantedSword()], positionChange=(12, 10),
			tileToMod=(12, 10), tileUnblock=True))


class MageLoft(HostileTile):
	def __init__(self):
		super().__init__(0, 0, 1, [Mage()])
		self.addEvent(Event(active=True, text=self.textElem[1].text))
		self.addEvent(Event(active=True, text=self.textElem[2].text))
		self.addEvent(Event(active=False, text=self.textElem[3].text))


# Shop tiles (!!!!ADD TEXT!!!!)
class David(ShopTile):
	def __init__(self):
		super().__init__(0, 0, "David's weapon shop", [BattleAxe(), Longsword()])
		self.addEvent(Event(active=False, text=self.textElem[3].text, items=[Torch],
			tileToMod=(10, 9), tileUnblock=True))


class Griffia(ShopTile):
	def __init__(self):
		super().__init__(0, 0, "Griffia's healing supplies", [Herbs(), SmallPotion(), HPotion(), StrongPotion()])


# Item tiles
class WestHerbsTile(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text, items=[Herbs(4)]))


class EastHerbsTile(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text, items=[Herbs(10)]))


class ShroomTile(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text, items=[Mushrooms(5)]))


class Laboratory(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text, items=[StrongPotion(3)]))


class StaffTile(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text, items=[Quarterstaff()]))


class SwordTile(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text, items=[Sword()]))


class WestForestGold(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text, items=[Gold(40)]))


class EastForestGold(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text, items=[Gold(100)]))


class DeadForestGold(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text, items=[Gold(200)]))


class CaveGold(Tile):
	def __init__(self):
		super().__init__(0, 0)
		self.addEvent(Event(active=True, text=self.textElem[1].text, items=[Gold(500)]))