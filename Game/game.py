from world import *
from tiles import HostileTile, ShopTile

def mainLoop():
	makeMap()

	while player.isAlive():
		# Get the current tile from map by player coordinates
		currentTile = tileMap.get((player.x, player.y))
		# Get available actions on this tile
		validActions = adjacentMoves(currentTile)
		validActions["i"] = "Check inventory - i"
		validActions["u"] = "Use / eqiup item - u"
		print("\n***********************************")
		# If there is an event on current tile, play it
		currentTile.event(player, tileMap)

		# Print text of current tile
		if not isinstance(currentTile, ShopTile):
			print(currentTile)
				
		# Set state of action
		state = 0
		if isinstance(currentTile, HostileTile):
			if currentTile.ambush():
				# Set state to fight
				state = 1

		elif isinstance(currentTile, ShopTile):
			# Set state to shop
			state = 2

		if state == 0:
			# Normal state            
			action = player.handleInput(validActions)
			text = player.doAction(action)
			if text:
				print(text)

		elif state == 1:
			# Fighting state
			currentTile.fight(player)

		elif state == 2:
			# Shopping state
			currentTile.shopping(player)

		# Increase player level, if he has enough xp
		print(player.levelUp())

	# Player died
	print("Game over!")


if __name__ == '__main__':
	mainLoop()