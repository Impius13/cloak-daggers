import xml.etree.ElementTree as et

import random
import inventory
from items import *

# Get text data from xml file
root = et.parse("data/textData.xml").getroot()

class Tile:
	def __init__(self, x, y):
		self.x = x
		self.y = y

		self.textElem = root.find(self.__class__.__name__)
		self.text = self.textElem[0].text
		# If event variable is set, an event would trigger upon first arrival on tile
		self.events = []

		# Variables for setting if the tile is blocked from specific sides
		self.eastBlocked = False
		self.westBlocked = False
		self.northBlocked = False
		self.southBlocked = False

	def __str__(self):
		return self.text

	def event(self, player, tileMap):
		# Play the first event from list, if it's active
		if self.events:
			if self.events[0].active:
				self.events[0].playEvent(player, tileMap)
				# Remove event form list afterwards
				del self.events[0]

	def addEvent(self, event):
		# Add an event to list
		self.events.append(event)
		

class HostileTile(Tile):
	def __init__(self, x, y, ambushChance, possibleEnemies):
		super().__init__(x, y)
		self.ambushChance = ambushChance
		self.enemyTypes = possibleEnemies

	def ambush(self):
		# Randomly determine if there will be an ambush
		if random.randint(1, self.ambushChance) == (int(self.ambushChance / 2)):
			return True
		return False

	def fight(self, player):
		# Randomly choose an enemy to fight from enemyTypes 
		enemy = random.choice(self.enemyTypes)
		validActions = {"a" : "Attack - a", "d" : "Defend - d",
			"i" : "Check inventory - i", "u" : "Use / eqiup item - u"}
		# Fight until either player or enemy is dead
		print("\nYou were ambushed!")
		while player.isAlive():
			# If it's the final fight, play event if the boss has 1/2 hp
			if self.__class__.__name__ == "MageLoft":
				if enemy.status() == "Hurt":
					self.event()

			# Check if the enemy died
			if enemy.status() == "Dead":
				# If it's the final fight, activate it's last event
				if self.__class__.__name__ == "MageLoft":
					self.events[0].active = True
					
				# Drop XP
				xpDrop = enemy.dropXp()
				player.xp += xpDrop
				print("{} died!".format(enemy.name))
				print("You've received {} experience.".format(xpDrop))
				if random.randint(1, 2) == 2:
					# Drop gold
					goldDrop = enemy.dropGold()
					player.inventory.addGold(goldDrop)
					print("You got {} gold coins!".format(goldDrop))
				break

			print(enemy.strFight())
			print("Your HP: {}".format(player.hp))

			# Player's turn
			action = player.handleInput(validActions)
			actionText = player.doAction(action, enemy)
			if actionText:
				print(actionText)

			# If it's the fight with Bear, add Balorg's turn
			if enemy.name == "Beast":
				balorgAttack = random.randint(5, 10)
				enemy.hp -= balorgAttack
				print("Balorg shot the {} and did {} damage!".format(enemy.name, balorgAttack))

			# Enemy's turn
			text = enemy.attack(player)
			if text:
				print(text)


class ShopTile(Tile):
	def __init__(self, x, y, name, saleItems):
		super().__init__(x, y)
		self.name = name
		# Make shop inventory
		self.inventory = inventory.Inventory(saleItems)

		# Get all trader phrases
		text = self.textElem.findall("text")
		phrases = []
		for phrase in text:
			phrases.append(phrase.text)

		self.text = phrases

	def __shopGreeting(self):
		# Return random greeting message from text
		return str(self.name + "\n\n" + random.choice(self.text))

	def shopping(self, player):
		# Shopping sequence
		validActions = {"b" : "Buy - b", "l" : "Sell - l", "w" : "Exit - w"}
		print("\n" + self.__shopGreeting())
		while True:
			action = player.handleInput(validActions)
			if action == "b":
				# Buying sequence
				# List items for sale
				print(self.inventory.listItemsShop())
				while True:
					print("\nYou have " + str(player.inventory.getGold()) + " gold.")
					choice = input("What would you want to buy? (type 'x' to exit): ")
					if self.inventory.getItem(choice):
						# Get the item and price
						item = self.inventory.getItem(choice)
						price = item.value
						# If player have enough gold for item
						if player.inventory.getGold() >= price:
							player.doAction(action, item)
							break

						else:
							print("\nYou can't afford that")

					elif choice == "x":
						# Exit from buying sequence
						break

					else:
						print("\nI don't have that for sale.")

			elif action == "l":
				# Selling sequence
				# List player inventory
				print("\nYour inventory:")
				print(player.inventory.listItems())
				while True:
					choice = input("\nWhat would you want to sell? (type 'x' to exit): ")
					item = player.inventory.getItem(choice)
					if item:
						if isinstance(item, Weapon):
							# Check if item is player's active weapon
							if item.active:
								print("\nYou can't sell your weapon.")
								continue
								
						# Get item price
						price = player.inventory.getItem(choice).value
						# Sell the item
						player.doAction(action, choice, price)

					elif choice == "x":
						# Exit from selling sequence
						break

					else:
						print("\nI don't have that on me.")

			elif action == "w":
				# Exit the shop
				player.doAction(action)
				break